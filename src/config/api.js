const HOST = process.env.VUE_APP_WEB_API

// master endpoint
export const register = `${HOST}/getregister`

// options endpoint
const optionsPrefix = 'options/'
export const options = {
  categories: `${HOST}${optionsPrefix}categories`
}
