import vuex from 'vuex'
import axios from 'axios'
import Vue from 'vue'
import { register } from '@/config/api'

Vue.use(vuex, axios)

export default new vuex.Store({
    state: {
      seps: []
    },
    actions: {
      loadSep ({commit}, data) {
        const params = {
          search: data.search,
          jns_rawat: data.jns_rawat,
          cara_bayar: data.cara_bayar,
          tgl_reg: data.tgl_reg
        }
        axios
          .get(`${register}/tanggal/search`, {params})
          .then(res => {
            console.log(res.data)
            let seps = res.data
            commit('SET_POSTS', seps)
          })
          .catch(error => {
            console.log(error)
          })
      }
    },
    mutations: {
      SET_POSTS (state, seps) {
        state.seps = seps
      }
    }
})