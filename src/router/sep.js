const Form = () => import('@/views/sep/Form.vue')

export default [
  {
    path: '/sep/buat/:reg',
    name: 'sep',
    component: Form,
    meta: { title: 'Pembuatan Sep' }
  }
]
